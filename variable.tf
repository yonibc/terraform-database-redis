variable "aws_region" {
  description = "The AWS region to deploy into (e.g. us-east-2)."
  default     = "us-east-2"
}

variable "replica_count" {
  description = "Number of cluster members."
}

variable "redis_cluster_id" {
  description = "Redis cluster name."
}

variable "subnet_group_name" {
  description = "Redis cluster subnet."
}

variable "security_group_ids" {
  description = "Redis security group."
}



