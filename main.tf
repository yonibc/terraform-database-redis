
resource "aws_elasticache_cluster" "cluster-redis" {
  count                = var.replica_count
  cluster_id           = "${var.redis_cluster_id[count.index]}"
  engine               = "redis"
  engine_version       = "3.2.6"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis3.2"
  port                 = 6379
  subnet_group_name    = "${var.subnet_group_name}"
  security_group_ids   = var.security_group_ids
}
